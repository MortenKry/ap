![Screenshot](art.png)

https://cdn.staticaly.com/gl/NicolaiRieder/ap/raw/master/Mini_ex9/index.html

For this weeks mini_ex we in group 3 collaborated: Morten, Caroline and Nicolai.

Spa**c**ial uoıʇɐxıɟ

This program has been made with the API provided by NASA on their website. We used the API's satellite function, which consists of a lot of images of earth taken by the Landsat 8 satellite.
We decided to load images from random locations (found via latitude and longtitude definitons) around the world. The longtitude and latitude are randomly chosen within a parameter of 0-360 and 0-90 respectively. This technically should only show us images from the northern hemisphere. We chose to limit it tot the northern hemisphere as we seemed to get a quite big amount of errors from the southern hemisphere and in general a lot of errors seem to occur when trying to call images: we believe this has something to do with a lack of images for certain coordinates.

NASA's API seemed to be the best choice for our purpose and while we would have preferred Google Maps API we simply didn't want to pay the premium.

We had a bit of struggle acquiring the photo's as (previously mentioned) some coordinates didn't have an image output, this meant that we would have to call the function again until it loaded a functioning image.
Throughout the development we had to rethink and reflect upon our ideas and the actual product we were programming. The end goal ended up shifting towards several alternative solutions until we ended up with the approach that can be seen above. We originally wanted to load several images in a grid and keep them in place (maybe about 6 images or so) but simply couldn't manage to figure out how to do it. After that, we tried to do several horisontal "lines" of images that, when changing to the column/line below, would also change to a new picture for the new line/column.
In the end we decided that a random placement approach would fit the randomness of the destinations better.


There are several things that can be included in the API query/request to have more influence over the outcome/return:
![Screenshot](apicalls.PNG)

While most of the query parameters are self-explanatory after having read NASA's description the *dim* and *date* functions require a bit of experimenting to fully understand.
While date itself seems fairly simple, not all dates have pictures and therefore makes it a tad harder to define a specific date for the query. While the dim parameter does what it says it doesn't give us any further idea of how to write these "degrees", nor how far 'up' one can go before the image is no longer quadratic. Every once in a while our program will get an image that is not quadratic and does not reach the size definitions defined by us, but without adjusting *dim* the results were simply too "pixely" for our taste.
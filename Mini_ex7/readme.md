https://cdn.staticaly.com/gl/NicolaiRieder/ap/raw/master/Mini_ex7/index.html
Refresh for generation of new art.

[!Screenshot](genart.png)
DISCLAIMER: Several parts of the code that appear in this mini_ex are taken from a generative artwork I found online, this I then edited to some degree so I could learn how to work with those pieces of code and add my own touch to the "program". Time has been a bit on the short side and next week should be a program created from the ground up by myself.

This weeks exercise revolved around creating code that would generate art. My approach to generative art was to set up a few "rules" so to speak that would define how the program would act when creating a visual object. The requirement was to create a cide that included both a for-loop and a conditional statement. 

Essentially, my program generates different instances of rectangles (rect()) and ellipses (ellipse()) of a random colour (Set within certain parameters). What shape is 'spawned' is randomised such as to create an ever greater difference between each instance of the program. I experimented with chosing a completely random colour for each shape with the random() syntax, yet I found it extremely obtrusive and hard to look at cause of all the colours.

All the shapes are drawn as vectors and the "drawing" mechanic is created via the walker function:
`function walker() {
  this.pos = createVector(width / 2, height / 2);
  this.vel = createVector(0, 0);

  this.update = function () {
    this.acc = createVector(random(-.1, .1), random(-.1, .1));
    this.vel.add(this.acc);
    this.pos.add(this.vel);
  };`
  

The rules I have defined in my program are implemented as to restrict the generation of shapes to a certain number. This I chose to do so the viewer can see what is going on, if an infinite amount of shapes were to be spawned in/generated then the artistic aspect would not be visible as the canvas would be covered in the colour quite quickly.

Generativity is the principle of creating, producing and well, generating some sort of output without any specific input required. In the context of my program this is to be understood as the shapes being created and those same shapes drawing on the canvas. I however, did not define where exactly these shapes would have to move which makes it a generative "program". 

Automation, to my understanding, is when a set job or objective can be achieved with minimal human effort. My program in itself is automatic in the sense that is is creating a drawing which I as the programmer wanted it to create. If I however, wanted the program to create something specific that would look the same everytime and it could not produce that then the program itself would not be automatic as it does to reach that goal. Automation is obvious in productional facilites around the world where i.e. new smartphones are being produced as every consumer should receive the same.
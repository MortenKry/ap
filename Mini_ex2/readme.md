![Screenshot](emoji2dx3d.PNG)

https://cdn.staticaly.com/gl/NicolaiRieder/ap/raw/master/Mini_ex2/p5/empty-example/index.html

I had a plan to make two versions of one emoji: one 2d and one 3d. The idea of this was to highlight that realism in regard to emojis is not always the best option. I chose to stick with the original yellow 'skin' tone for both versions. The 2d smiley was constructed using the circle() and curve() syntaxes as I wanted to create the 3d smiley from 'the ground up'. Previous to this I had not used the curve() syntax so figuring out what number did what turned out to be quite the struggle. I am still not quite sure what the different x and y positions each define (curve(x1, y1, x2, y2, x3, y3, x4, y4). 

After using some of the syntaxes that I previously used in Mini_Ex1 (such as fill(), translate() and stroke()) I ended up having a completed emoji, a somewhat happy/cheeky smiley. Now came the time for me to do its 3d counterpart, for which I used ellipsoid() and torus(), used respectively for the head plus the eyes and the mouth. The horrifying creation hereby came to be, a bit crazy looking happy smiley, in 3d of course. 

I decided that since I did not need any animation of the canvas I could simply put all the code in the function setup() area of my code; It would simply be unnecessary for the emojis to be re-drawn every time function draw() would repeat.  

I see these emojis as a critique to some of the debate that has been nourished by some of the 'skin colour' changes of existing emojis. I tried to portray this by showing of that a 3d version i.e. a more lifelike version of a smiley/emoji is not always something that should be preferred. Sometimes things are really just as they seem, a yellow emoji is not always meant to represent something other than what it is, an emoji. Not everything has to be offensive unless we decide to add that connotation to it.
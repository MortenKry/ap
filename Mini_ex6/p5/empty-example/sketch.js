var paddle, catchthis, dontcatch, wallTop, wallBottom, wallLeft, wallRight;
var bricks;
var MAX_SPEED = 9;
var WALL_THICKNESS = 30;
var BRICK_W = 40;
var BRICK_H = 20;
var BRICK_MARGIN = 4;
var ROWS = 9;
var COLUMNS = 16;
var i = 0

function setup() {
  createCanvas(800, 600);

  paddle = createSprite(width/2, height-50, 100, 10);

  paddle.immovable = true;


  wallBottom = createSprite(width/2, height+WALL_THICKNESS/2, width+WALL_THICKNESS*2, WALL_THICKNESS);
  wallBottom.immovable = true;

  paddle.shapeColor = color(255, 255, 255);
  lastPrint = millis() - 1000;
  textSize(40);
  textAlign(CENTER, CENTER);




}

function draw() {
  background(0, 134, 131);
  var timeElapsed = millis() - lastPrint;

  if (timeElapsed > 1000) {
    catchthis = new Positive();
    dontcatch = new Negative();
    text('tricked, this doesn\'t work', width/2, height/2)

   console.log(i);
   lastPrint = millis();
}

  catchthis.move();
  catchthis.display();
  dontcatch.move();
  dontcatch.display();

  paddle.position.x = constrain(mouseX, paddle.width/2, width-paddle.width/2);

  drawSprites();

}

class Positive {
  constructor() {
    this.x = random(width);
    this.y = 0;
    this.diameter = random(10,30);
    this.speed = random(1, 5);
    this.setCollider = 'circle', 0, 0, this.diameter/2;

  }

  move() {
    this.y += this.speed;
  }

  display() {
    push();
    fill(0, 255, 0)
    ellipse(this.x, this.y, this.diameter, this.diameter);
    pop();
  }
}
class Negative {
  constructor() {
    this.x = random(width);
    this.y = 0;
    this.diameter = random(10, 30);
    this.speed = random(1, 5);
    this.setCollider = 'circle', 0, 0, this.diameter/2;
  }

  move() {
    this.y += this.speed;
  }

  display() {
    push()
    fill(255, 0, 0)
    ellipse(this.x, this.y, this.diameter, this.diameter);
    pop();
  }
}
